FROM node:10 AS ui-build
WORKDIR /usr/src/app
COPY front/my-app/ ./my-app/
RUN cd my-app && npm install && npm run build

FROM node:10 AS server-build
WORKDIR /root/
COPY --from=ui-build /usr/src/app/my-app/build ./my-app/build
COPY service/package*.json ./service/
RUN cd service && npm install
COPY service/app.js ./service/

EXPOSE 3050

CMD ["node", "./service/app.js"]
